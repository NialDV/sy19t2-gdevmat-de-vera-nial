void setup() 
{
  size(1780, 920, P3D);
  camera(0, 0, -(height /2.0f) / tan(PI * 30/180), 0, 0, 0, 0, -1, 0); 
  background(255);
  noStroke();
}
  float dt = 0;
  float xPos;
  float yPos;
  float size = map(noise(30), 0, 1, 30, 120);
  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  float alpha_t = 200;
  //float r_t = map(noise(100), 0, 1, 0, 255);
  //float g_t = map(noise(100), 0, 1, 0, 255);
  //float b_t = map(noise(100), 0, 1, 0, 255);
  //float alpha_t = map(noise(100), 0, 1, 0, 255);    

  void render() 
  {
    circle(xPos, yPos, size);  
    fill (map(noise(r_t), 0, 1, 0, 255), map(noise(g_t), 0, 1, 0, 255), map(noise(b_t), 0, 1, 0, 255), map(noise(alpha_t), 0, 1, 0, 255));
    //fill (r_t, g_t, b_t, alpha_t);
    r_t += 0.1f;  
    g_t += 0.1f;
    b_t += 0.1f;
    alpha_t += 0.01f;
  }
  
  void randomWalker()
  {
    float x = noise(dt);
    float y = noise(dt + 5);
    xPos = map(x, 0, 1, Window.left, Window.right);
    yPos = map(y, 0, 1, Window.bottom, Window.top);
    dt += 0.01f;
  }
  
void draw() 
  {
    render();
    randomWalker();
  }
