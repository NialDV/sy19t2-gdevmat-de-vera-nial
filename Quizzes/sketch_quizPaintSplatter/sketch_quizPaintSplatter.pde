void setup() 
{
  size(1780, 920, P3D);
  camera(0, 0, -(height /2.0f) / tan(PI * 30/180), 0, 0, 0, 0, -1, 0); 
  noStroke();
  background(0);
}

 long resetTime = 0;

void draw() 
{
  float gauss = randomGaussian();
  float standardDeviation = 240;
  float mean = 0;
  
  float xPos = (standardDeviation * gauss) + mean;
  float yPos = random(1000) + (-height/2);
  float size = random(100);
  
 //noStroke();
  color col = color(floor(random(255)), floor(random(255)), floor(random(255)), floor(random(100)));
  alpha(col);
  fill(col);
  circle(xPos, yPos, size);
  
  println(frameCount);
  if(frameCount % 1000 == 0) { 
  background(0);
  }
  
}
