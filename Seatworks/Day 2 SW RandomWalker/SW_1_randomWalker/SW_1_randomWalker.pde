//What is randomness - lacking a pattern or principle of organiztion


void setup() 
{
  size(1780, 920, P3D);
  camera(0, 0, -(height /2.0) / tan(PI * 30/180), 0, 0, 0, 0, -1, 0); 
  noStroke();
  background(255);
}

  Walker walker = new Walker();
  
void draw() 
{
  walker.render();
  walker.randomWalker();
  
}
