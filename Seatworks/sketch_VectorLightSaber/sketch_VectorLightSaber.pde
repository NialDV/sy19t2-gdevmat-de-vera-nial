void setup() 
{
  size(1780, 920, P3D);
  camera(0, 0, -(height /2.0) / tan(PI * 30/180), 0, 0, 0, 0, -1, 0); 
  noStroke();
}

Vector2 mousePos()
{
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  return new Vector2(x, y);
}

  float r_t = 100;
  float g_t = 150;
  float b_t = 200;
  float alpha_t = 200;
  float d_t = 69;
  
void draw()
{
  background(0);
  
  Vector2 mouse = mousePos();
  mouse.normalize();
  mouse.mult(400);
  
  strokeWeight(15);
  rotate(map(noise(d_t), 0, 1, 0, 2 * PI));
  
  //White
  stroke(200,200,200,200);
  strokeWeight(3);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  //Glow
  stroke(map(noise(r_t), 0, 1, 0, 255), map(noise(g_t), 0, 1, 0, 255), map(noise(b_t), 0, 1, 0, 255), map(noise(alpha_t), 0, 1, 0, 255));
  r_t += 0.1f;  
  g_t += 0.1f;
  b_t += 0.1f;
  alpha_t += 0.01f;
  d_t += 0.01f;  
  strokeWeight(10);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  //Handle
  mouse.div(6);
  stroke(20,20,20);
  strokeWeight(15);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  //Middle Red
  mouse.div(20);
  stroke(200,10,10);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
  //Black Outline
  mouse.div(2);
  stroke(10,10,10);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, -mouse.x, -mouse.y);
  
}
