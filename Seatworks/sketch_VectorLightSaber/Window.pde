public static class Window
{
  public static  int widthPx = 1780;
  public static  int heightPx = 920;
  public static  int windowWidth = widthPx / 2;
  public static  int windowHeight = heightPx / 2;
  public static  int top = windowHeight;
  public static  int bottom = -windowHeight;
  public static  int left = -windowWidth;
  public static  int right = windowWidth;
}
